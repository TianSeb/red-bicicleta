let Bicicleta = require("../../models/bicicleta.js"); //Los modelos se requieren!!!! no son variables, tengo que REQUERIR!!!

beforeEach(()=>{ Bicicleta.allBicis = []}); //método de jasmine para no repetir código, esto lo ejecuta antes de cada test.

describe("Bicicletas.allBicis", () => { // Aca le pongo en "" lo que estoy testeando, para dar una idea de que estoy armando.
    it("comienza vacía", () => {
        expect(Bicicleta.allBicis.length).toBe(0); //  Aca le digo en it que espero que este array arranque en 0. (toBe es lo que indica)
    }); // para testearlo puedo hacer NPM test (llama al script) o jasmine + la ruta (spec/models/bicicleta_test_spec.js)
});

describe("Bicicleta.add", () => {
    it("agrega bici", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta (1, "rojo", "urbana", [-34.590095, -58.393644]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.findById", () => {
    it("find debe devolver la bici con id 1", ()=>{
        expect(Bicicleta.allBicis.length).toBe(0); //repite esto como medida de chequeo.
        var aBici = new Bicicleta (1, "verde", "urbana", [-34.590095, -58.393644]);
        var aBici2 = new Bicicleta (2, "azul", "campestre", [-34.590095, -58.393644]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        let targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    });

});